"use client"

import React from 'react';
import {ConfigProvider} from 'antd';

import {PageContainer, ProCard} from '@ant-design/pro-components';

import './page.module.css';


export default function Home() {
  return (
    <React.StrictMode>
      <ConfigProvider>
        <PageContainer
          header={{
            title: "home",
            //extra: props.extra,
          }}
          subTitle={"props.subTitle"}
        >
          <ProCard
            className={`mb-10 shadow-lg`}
            size="small"
            style={{minHeight: 500}}
            ghost={true}
          >

          </ProCard>
        </PageContainer>
      </ConfigProvider>
    </React.StrictMode>
  );
}
